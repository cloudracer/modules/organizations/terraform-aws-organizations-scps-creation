## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_policy.all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_scps"></a> [scps](#input\_scps) | Map of all SCPs | `map` | `{}` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags for all AWS Resources of that Module | `map` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_scps_list"></a> [scps\_list](#output\_scps\_list) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
