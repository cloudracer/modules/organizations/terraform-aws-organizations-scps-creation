resource "aws_organizations_policy" "all" {
  for_each = var.scps

  name        = "scp-${each.key}-tflz"
  description = "Generated SCP from Terraform Landingzone: ${each.key}"
  content     = jsonencode(each.value)

  tags = local.tags
}
