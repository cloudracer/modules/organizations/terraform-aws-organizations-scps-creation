variable "scps" {
  description = "Map of all SCPs"
  default     = {}
}

# Other #
variable "tags" {
  description = "Tags for all AWS Resources of that Module"
  default     = {}
}
