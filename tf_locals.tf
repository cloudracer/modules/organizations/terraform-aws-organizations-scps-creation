locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-organizations-scps-creation"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-organizations-cps-creation"
  }
  tags = merge(local.tags_module, var.tags)
}
